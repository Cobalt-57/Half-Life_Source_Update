Half-Life-Source-Update
==================================
This is the GitLab for the Half-Life Source Update. 

This is used for reporting bugs. When reporting bugs, be sure to add a label infront of your report.


Game Labels:
==================================
[HL1S] - For Half-Life: Source Bugs

[HLDMS] - For Half-Life Deathmatch Source Bugs

[TFCS] - For Team Fortress Classic Source Bugs

[DMCS] - For Deathmatch Classic Source Bugs

[CSCS] - For Counter-Strike Classic Source Bugs


Engine Labels:
==================================
[Update] - This Project

[Steampipe] - Current Build in the HLS/HLDMS Depot Currently (2013 update)

[Legacy] - Build that was in the Depot from 2004-2013 (Source Base 2006 version) Download: http://steamcommunity.com/groups/legacy_is_cancel 

[Global] - This states that the bug is present in both versions of Half-Life: Source (Legacy and Steampipe)
