#include "cbase.h"
#include "hlsu_gamerules.h"
#include "viewport_panel_names.h"
#include "gameeventdefs.h"
#include <KeyValues.h>
#include "ammodef.h"

#ifdef CLIENT_DLL
#include "c_hlsu_player.h"
#else

#include "eventqueue.h"
#include "player.h"
#include "gamerules.h"
#include "game.h"
#include "items.h"
#include "entitylist.h"
#include "mapentities.h"
#include "in_buttons.h"
#include <ctype.h>
#include "voice_gamemgr.h"
#include "iscorer.h"
#include "hlsu_player.h"
#include "team.h"
#include "voice_gamemgr.h"
#include "hlsu_gameinterface.h"

#ifdef DEBUG	
#include "hlsu_bot_temp.h"
#endif

#endif

REGISTER_GAMERULES_CLASS(CHLSUGameRules);

BEGIN_NETWORK_TABLE_NOBASE(CHLSUGameRules, DT_HLSUGameRules)

#ifdef CLIENT_DLL

#else

#endif

END_NETWORK_TABLE()


LINK_ENTITY_TO_CLASS(hlsu_gamerules, CHLSUGameRulesProxy);
IMPLEMENT_NETWORKCLASS_ALIASED(HLSUGameRulesProxy, DT_HLSUGameRulesProxy)

#ifdef CLIENT_DLL
void RecvProxy_HLSUGameRules(const RecvProp *pProp, void **pOut, void *pData, int objectID)
{
	CHLSUGameRules *pRules = HLSUGameRules();
	Assert(pRules);
	*pOut = pRules;
}

BEGIN_RECV_TABLE(CHLSUGameRulesProxy, DT_HLSUGameRulesProxy)
RecvPropDataTable("hlsu_gamerules_data", 0, 0, &REFERENCE_RECV_TABLE(DT_HLSUGameRules), RecvProxy_HLSUGameRules)
END_RECV_TABLE()
#else
void* SendProxy_HLSUGameRules(const SendProp *pProp, const void *pStructBase, const void *pData, CSendProxyRecipients *pRecipients, int objectID)
{
	CHLSUGameRules *pRules = HLSUGameRules();
	Assert(pRules);
	return pRules;
}

BEGIN_SEND_TABLE(CHLSUGameRulesProxy, DT_HLSUGameRulesProxy)
SendPropDataTable("hlsu_gamerules_data", 0, &REFERENCE_SEND_TABLE(DT_HLSUGameRules), SendProxy_HLSUGameRules)
END_SEND_TABLE()
#endif

CHLSUGameRules::CHLSUGameRules()
{
#ifndef CLIENT_DLL

#endif
}

CHLSUGameRules::~CHLSUGameRules(void)
{
#ifndef CLIENT_DLL
	g_Teams.Purge();
#endif
}

void CHLSUGameRules::Think(void)
{

#ifndef CLIENT_DLL

	BaseClass::Think();

#endif
}

void CHLSUGameRules::Precache(void)
{
	BaseClass::Precache();
}

bool CHLSUGameRules::ClientCommand(CBaseEntity *pEdict, const CCommand &args)
{

	return BaseClass::ClientCommand(pEdict, args);

}

void CHLSUGameRules::CreateStandardEntities(void)
{
#ifndef CLIENT_DLL
	BaseClass::CreateStandardEntities();
	CBaseEntity::Create("hlsu_gamerules", vec3_origin, vec3_angle);
#endif
}