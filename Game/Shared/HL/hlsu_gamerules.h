#ifndef HLSU_GAMERULES_H
#define HLSU_GAMERULES_H
#pragma once

#include "gamerules.h"
#include "teamplay_gamerules.h"
#include "gamevars_shared.h"

#ifndef CLIENT_DLL
#include "hlsu_player.h"
#endif

#ifdef CLIENT_DLL
#define CHLSUGameRules C_HLSUGameRules
#define CHLSUGameRulesProxy C_HLSUGameRulesProxy
#endif

class CHLSUGameRulesProxy : public CGameRulesProxy
{
public:
	DECLARE_CLASS(CHLSUGameRulesProxy, CGameRulesProxy);
	DECLARE_NETWORKCLASS();
};

class CHLSUGameRules : public CTeamplayRules
{
public:
	DECLARE_CLASS(CHLSUGameRules, CTeamplayRules);

#ifdef CLIENT_DLL

	DECLARE_CLIENTCLASS_NOBASE();

#else

	DECLARE_SERVERCLASS_NOBASE();
#endif

	CHLSUGameRules();
	virtual ~CHLSUGameRules();

	virtual void Precache(void);
	virtual bool ClientCommand(CBaseEntity *pEdict, const CCommand &args);

	virtual void Think(void);

	virtual const char *GetGameDescription(void){ return "Half-Life: Source Update"; }
	virtual void CreateStandardEntities(void);

#ifndef CLIENT_DLL


#endif

private:

};

inline CHLSUGameRules* HLSUGameRules()
{
	return static_cast<CHLSUGameRules*>(g_pGameRules);
}

#endif